package ru.spbpu.ics.dao.impls;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import ru.spbpu.ics.dao.interfaces.PersonDao;
import ru.spbpu.ics.dao.interfaces.PersonTable;
import ru.spbpu.ics.dao.logic.Person;

import java.util.List;

@Component("personSQL")
public class PersonSql implements PersonDao, PersonTable {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public void insert(Person person) {
        String sql = "INSERT INTO person (first_name, last_name, district, birth) VALUES (?, ?, ?, ?)";
        jdbcTemplate.update(sql, new Object[]{person.getFirstName(), person.getLastName(), person.getDistrict(),
                person.getBirth()});
    }

    public List<Person> getAll() {
        String sql = "SELECT * FROM PERSON";
        return jdbcTemplate.query(sql, new BeanPropertyRowMapper<>(Person.class));
    }

    public void deleteByID(int id) {
        String deleteByID = "DELETE FROM person WHERE id = ?";
        jdbcTemplate.update(deleteByID, id);
    }

    public void updateByID(int id, Person person) {
        String updateByID = "UPDATE person SET first_name = ?, last_name = ?, birth = ?, district = ?  WHERE id = ?";
        jdbcTemplate.update(updateByID, person.getFirstName(),
                person.getLastName(), person.getBirth(), person.getDistrict(), id);
    }

    public Person getByID(int id) {
        String sql = "SELECT * FROM PERSON WHERE ID = ?";
        return jdbcTemplate.queryForObject(sql, new Object[]{id},
                new BeanPropertyRowMapper<>(Person.class));
    }

    public List<Integer> getAllbyDistrict(String district) {
        String sql = "SELECT ID FROM PERSON WHERE district = " + district;
        return jdbcTemplate.queryForList(sql, Integer.class);
    }

    public List<Integer> getAllbyDate(String date) {
        String sql = "SELECT ID FROM PERSON WHERE birth = \'" + date + "\'";
        return jdbcTemplate.queryForList(sql, Integer.class);
    }

    public void deleteAll() {
        String deleteAllPerson = "DELETE FROM PERSON";
        jdbcTemplate.update(deleteAllPerson);
    }

    public void drop() {
        String drop = "DROP TABLE IF EXISTS person";
        jdbcTemplate.execute(drop);
    }

    @Override
    public void create() {
        String create = "CREATE TABLE person " + "(id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, "
                + "first_name TEXT NOT NULL, " + "last_name TEXT NOT NULL, " + "birth DATETIME NOT NULL," + "district TEXT NOT NULL);";
        jdbcTemplate.execute(create);
    }
}
