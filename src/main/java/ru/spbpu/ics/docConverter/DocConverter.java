package ru.spbpu.ics.docConverter;

import org.apache.poi.xwpf.usermodel.*;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTblWidth;
import ru.spbpu.ics.dao.logic.Person;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.util.List;

/**
 * Created by adm on 23.04.2017.
 */
public class DocConverter {
    private static DocConverter ourInstance = new DocConverter();

    public static DocConverter getInstance() {
        return ourInstance;
    }

    private static void setRun(XWPFRun run,
                               int fontSize, String text, boolean bold, boolean addBreak) {
        //run.setFontFamily(fontFamily);
        run.setFontSize(fontSize);
        run.setText(text);
        run.setBold(bold);
        if (addBreak) run.addBreak();
    }

    private static void styleText(XWPFTableRow row, String text, int index, boolean bold) {
        XWPFParagraph paragraph = row.getCell(index).addParagraph();
        setRun(paragraph.createRun(), 12, text, bold, false);
    }

    public static void printToDoc(File file, List<Person> members) throws IOException {
        XWPFDocument document = new XWPFDocument();
        XWPFTable tableOne = document.createTable(1, members.get(0).getFieldsOnjects().length);
        XWPFTableRow headersRow = tableOne.getRow(0);

        styleText(headersRow, "Номер", 0, true);
        styleText(headersRow, "Фамилия", 1, true);
        styleText(headersRow, "Имя", 2, true);
        styleText(headersRow, "Район", 3, true);
        styleText(headersRow, "Дата рождения", 4, true);
        CTTblWidth width = tableOne.getCTTbl().addNewTblPr().addNewTblW();
        for (int i = 0; i < members.get(0).getFieldsOnjects().length; i++) {
            headersRow.getCell(i).getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf(2000));
        }

        for (Person person : members) {
            XWPFTableRow row = tableOne.createRow();
            row.setHeight(200);
            for (int i = 0; i < person.getFieldsOnjects().length; i++) {
                row.getCell(i).setText(person.getFieldsOnjects()[i].toString());
            }
        }
        FileOutputStream outStream = new FileOutputStream(file);
        document.write(outStream);
        outStream.close();
    }

    private DocConverter() {
    }
}
