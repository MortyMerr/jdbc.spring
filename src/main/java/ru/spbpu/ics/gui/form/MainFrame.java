package ru.spbpu.ics.gui.form;

import com.github.lgooddatepicker.components.DatePicker;
import org.apache.commons.io.FilenameUtils;
import ru.spbpu.ics.dao.logic.Person;
import ru.spbpu.ics.docConverter.DocConverter;
import ru.spbpu.ics.gui.table_model.PersonModel;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

/**
 * Created by adm on 17.04.2017.
 */
public class MainFrame extends JFrame {
    private JTable table1;
    private JPanel MainPanel;
    private JButton применитьButton;
    private JTextField district;
    private JTextField имяTextField;
    private JTextField фамилияTextField;
    private DatePicker birth;
    private JTextField районTextField;
    private JButton добавитьButton;
    private DatePicker filterdate;
    private JButton сохранитьButton;
    private JButton сохранитьПоУмолчаниюButton;
    private void clearAddForm(){
        имяTextField.setText("");
        фамилияTextField.setText("");
        районTextField.setText("");
        birth.setText("");
    }

    public MainFrame() {
        super();
        PersonModel model = new PersonModel();
        table1.setModel(model);
        setContentPane(MainPanel);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
        setVisible(true);
        применитьButton.addActionListener(e -> {
            model.refresh();
            if (!district.getText().isEmpty()) {
                model.filterByDistrict(district.getText());
            }
            if (!filterdate.getText().isEmpty()) {
                model.filterByDate(filterdate.getDate().toString());
            }
            table1.updateUI();
        });

        добавитьButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!имяTextField.getText().isEmpty()
                        && !фамилияTextField.getText().isEmpty()
                        && !районTextField.getText().isEmpty()
                        && !birth.getText().isEmpty()) {
                    /*Person person = new Person();
                    person.setBirth(Date.valueOf(birth.getDate()));
                    person.setDistrict(районTextField.getText());
                    person.setFirstName(имяTextField.getText());
                    person.setLastName(фамилияTextField.getText());
                    model.sqLite.insert(person);*/
                    model.sqLite.insert(new Person(имяTextField.getText(), фамилияTextField.getText(), районTextField.getText(),
                            birth.getDate().toString()));
                    model.refresh();
                    clearAddForm();
                    table1.updateUI();
                }
            }
        });

        сохранитьButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser fc = new JFileChooser();
                fc.setFileFilter(new FileNameExtensionFilter("Microsoft Word File (*.doc)", "doc"));
                if (fc.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
                    try {
                        File file = fc.getSelectedFile();
                        if (!FilenameUtils.getExtension(file.getName()).equalsIgnoreCase("doc")) {
                            file = new File(file.toString() + ".doc");
                        }
                        DocConverter.printToDoc(file, model.getMembers());
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }
            }
        });

        сохранитьПоУмолчаниюButton.addActionListener(e -> {
            try {
                File file = new File("document.doc");
                file.createNewFile();
                DocConverter.printToDoc(file, model.getMembers());
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        });
    }
}
