package ru.spbpu.ics.gui.table_model;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.spbpu.ics.dao.config.SqlConfig;
import ru.spbpu.ics.dao.impls.PersonSql;
import ru.spbpu.ics.dao.logic.Person;

import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;
import java.io.File;
import java.lang.reflect.Field;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by adm on 17.04.2017.
 */
public class PersonModel extends AbstractTableModel implements TableModel {
    private ApplicationContext context2 = new AnnotationConfigApplicationContext(SqlConfig.class);
    public PersonSql sqLite = (PersonSql) context2.getBean("personSQL");

    {
        if (!new File("db/SpringDB.db").exists()) {
            sqLite.create();
        }
    }

    public void setSqLite(PersonSql sqLite) {
        this.sqLite = sqLite;
    }

    private List<Person> members = sqLite.getAll();
    private Set<TableModelListener> listeners = new HashSet<TableModelListener>();

    public List<Person> getMembers(){
        return members;
    }
    @Override
    public int getRowCount() {
        return members.size();
    }

    @Override
    public int getColumnCount() {
        if (members.isEmpty())
            return 0;
        return members.get(0).getClass().getDeclaredFields().length;
    }

    @Override
    public String getColumnName(int columnIndex) {
        return members.get(0).getClass().getDeclaredFields()[columnIndex].getName();
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return members.get(0).getClass();
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return (columnIndex > 0);
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        return members.get(rowIndex).getFieldsOnjects()[columnIndex];
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        Field[] tmp = members.get(rowIndex).getClass().getFields();
        fireTableCellUpdated(rowIndex, columnIndex);
        //FIXME do we need pushing data
    }

    @Override
    public void addTableModelListener(TableModelListener l) {
        listeners.add(l);
    }

    @Override
    public void removeTableModelListener(TableModelListener l) {
        listeners.remove(l);
    }

    private void filterBy(List<Integer> filtered) {
        members = members.stream()
                .filter(m -> filtered.contains(m.getId()))
                .collect(Collectors.toList());
    }

    public void filterByDate(String date) {
        filterBy(sqLite.getAllbyDate(date));
        //FIXME check
    }

    public void refresh() {
        members = sqLite.getAll();
    }

    public void filterByDistrict(String district) {
        filterBy(sqLite.getAllbyDistrict(district));
    }
}
